import {
  render,
  screen,
  fireEvent,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import App from "./App";
import FormComponent from "./components/form";
import ComponentWithProps from "./components/props";
import user from "@testing-library/user-event";
import FetchComponent from "./components/fetch";

// test("renders learn react link", () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

it("render count state correctly", () => {
  const { getByText, container } = render(<App />);
  const text = container.firstChild;
  console.log("txt", text.textContent);
  expect(text).toHaveTextContent("Count : 0");
  const inc = getByText("+");
  const dec = getByText("-");
  fireEvent.click(inc);
  expect(text).toHaveTextContent("Count : 1");
  fireEvent.click(inc);
  expect(text).toHaveTextContent("Count : 2");
  fireEvent.click(dec);
  expect(text).toHaveTextContent("Count : 1");
});

it("render props correctly", () => {
  const { getByText, rerender } = render(
    <ComponentWithProps text={"this is a props"} />
  );
  expect(getByText("this is a props")).toBeInTheDocument();
  rerender(<ComponentWithProps text={"new props"} />);
  expect(getByText("new props")).toBeInTheDocument();
});

it("show name on change", () => {
  const { getByText, getByPlaceholderText } = render(<FormComponent />);
  const input = getByPlaceholderText(/input your name/i);
  user.type(input, "yogie");
  expect(getByText(/your name is yogie/i)).toBeInTheDocument();
});

// it("dropdown input", () => {
//   const { getByText, getByTestId } = render(<FormComponent />);
//   const input = getByTestId(/drops/i);
//   fireEvent.click(input);
//   fireEvent.click(getByText("2"));
//   expect(getByText(/your pick is 2/i)).toBeInTheDocument();
//   screen.debug();
// });

it("fetch data correctly", async () => {
  const { getByText } = render(<FetchComponent />);
  expect(getByText(/loading/i)).toBeInTheDocument();
  await waitForElementToBeRemoved(() => getByText(/loading/i));
  expect(getByText(/Leanne Graham/i)).toBeInTheDocument();
});
