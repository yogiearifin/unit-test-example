import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import ComponentWithProps from "./components/props";
import FormComponent from "./components/form";
import FetchComponent from "./components/fetch";

function App() {
  const [count, setCount] = useState(0);
  return (
    <div className="App">
      <p>Count : {count}</p>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
      <ComponentWithProps />
      <FormComponent />
      <FetchComponent />
    </div>
  );
}

export default App;
