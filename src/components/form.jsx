import { useState } from "react";

const FormComponent = () => {
  const [name, setName] = useState("");
  const [drop, setDrop] = useState("");
  return (
    <div>
      <label>Name</label>
      <input
        type="text"
        placeholder="input your name"
        value={name}
        onChange={(e) => setName(e.target.value)}
        name="name"
      />
      {name ? <h1>Your name is {name}</h1> : null}
      {/* did not passed the coverage */}
      {/* <select name="drops" onChange={(e) => setDrop(e.target.value)} data-testid="drops">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select>
      {drop ? <h1>Your pick is {drop}</h1> : null} */}
    </div>
  );
};

export default FormComponent;
