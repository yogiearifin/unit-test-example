import { useState, useEffect } from "react";

const FetchComponent = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((datas) => setData(datas));
  }, []);
  return (
    <div>
      <h1>Fetch</h1>
      {data.length ? (
        data &&
        data.map((item, index) => {
          return (
            <div key={index}>
              <p>{item.name}</p>
            </div>
          );
        })
      ) : (
        <h1>Loading</h1>
      )}
    </div>
  );
};

export default FetchComponent;
